Lunchtime is a Asp.Net Core web-api where you can submit the food you'd like during lunch

# Running the app
Start everything locally with `docker-compose up`  
Reload changes with: `docker-compose up -d --no-deps --build web`  


For more information on debugging and running .net core with containers [read the docs](https://code.visualstudio.com/docs/containers/debug-netcore)


# Getting the tools:
For code generation (controllers): `dotnet tool install --global aspnet-codegenerator`
For handling migrations:`dotnet tool install --global dotnet-ef`
For using mysql you need to install the mysql package `dotnet add package MySql.Data.EntityFrameworkCore`

(Make sure you have added and restored the required packages! (Found in LunchTime.csproj))

# Migrations
[Overview for ef core can be found here](https://docs.microsoft.com/en-us/ef/core/miscellaneous/cli/dotnet#dotnet-ef-migrations-remove)
1. Create models in the models folder. 
2. Link these in startup.cs with dbContext. 
3. Then `dotnet ef migrations add InitialCreate -v`
4. Run `dotnet ef database update` to run the migrations. If you get an error concering: EFMigrationHistory doesn't exist, remove the database. And run the command again, there is a bug if the database is initialized manually, without this table



# Scaffolding
Dotnet has a code generator for controllers. I have yet to determine the limits and boundaries, but for a simple, crud class it functions well.
Example command: `dotnet aspnet-codegenerator controller -name LunchItemsController -async -api -m LunchItem -dc LunchItemContext -outDir controllers`