using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LunchTime.Models;

namespace LunchTime.controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LunchItemsController : ControllerBase
    {
        private readonly LunchItemContext _context;

        public LunchItemsController(LunchItemContext context)
        {
            _context = context;
        }

        // GET: api/LunchItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LunchItem>>> GetLunchItems()
        {
            return await _context.LunchItems.ToListAsync();
        }

        // GET: api/LunchItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LunchItem>> GetLunchItem(string id)
        {
            var lunchItem = await _context.LunchItems.FindAsync(id);

            if (lunchItem == null)
            {
                return NotFound();
            }

            return lunchItem;
        }

        // PUT: api/LunchItems/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLunchItem(long id, LunchItem lunchItem)
        {
            if (id != lunchItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(lunchItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LunchItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LunchItems
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<LunchItem>> PostLunchItem(LunchItem lunchItem)
        {
            _context.LunchItems.Add(lunchItem);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LunchItemExists(lunchItem.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetLunchItem", new { id = lunchItem.Id }, lunchItem);
        }

        // DELETE: api/LunchItems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<LunchItem>> DeleteLunchItem(string id)
        {
            var lunchItem = await _context.LunchItems.FindAsync(id);
            if (lunchItem == null)
            {
                return NotFound();
            }

            _context.LunchItems.Remove(lunchItem);
            await _context.SaveChangesAsync();

            return lunchItem;
        }

        private bool LunchItemExists(long id)
        {
            return _context.LunchItems.Any(e => e.Id == id);
        }
    }
}
