using Microsoft.EntityFrameworkCore;

namespace LunchTime.Models
{
    public class LunchItem
    {
        public long Id {get; set;}
        public string Name {get; set;}
        public string Description {get;set;}

    }
}