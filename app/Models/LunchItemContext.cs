using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace LunchTime.Models
{
    public class LunchItemContext : DbContext
    {
        public LunchItemContext(DbContextOptions<LunchItemContext> options) : base(options)
        {

        }

        public DbSet<LunchItem> LunchItems { get; set; }

        // Can be used to seed data
        // Is meant for data which should always be in a DB, like postal codes
        // NOT for test data. As this will always be included.
        // https://docs.microsoft.com/en-us/ef/core/modeling/data-seeding
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // modelBuilder.Entity<LunchItem>().HasData(this.CreateTestSeedData());
        }

        private LunchItem[] CreateTestSeedData()
        {
            return new LunchItem[] {
                    new LunchItem() { Id= 1, Name = "Peanutbutter", Description = "Calve is the only option" },
                    new LunchItem() { Id= 2, Name = "Chicken samourai salad", Description = "Ancient japanese" }
            };
            // Since a PK is required for EF we provide it rather than allow the DB to generate it.
        }
    }
}